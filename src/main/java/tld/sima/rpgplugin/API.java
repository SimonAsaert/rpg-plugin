package tld.sima.rpgplugin;

import java.util.UUID;

import tld.sima.rpgplugin.player.CharacterList;
import tld.sima.rpgplugin.server.RaceInformation;

public class API {
	
	Main plugin = Main.getPlugin(Main.class);
	// Get current list of characters assigned to player
	public CharacterList getCharacterList(UUID uuid) {
		return plugin.getCharacterList().get(uuid);
	}
	// Get the entire race information
	public RaceInformation getRace(String race) {
		return plugin.getRace().get(race);
	}
}
