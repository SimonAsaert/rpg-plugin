package tld.sima.rpgplugin.events;

import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import tld.sima.rpgplugin.Main;
import tld.sima.rpgplugin.inventories.CharacterInfoMenu;
import tld.sima.rpgplugin.inventories.MainCharacterMenu;
import tld.sima.rpgplugin.player.CharacterInfo;
import tld.sima.rpgplugin.server.Buff;
import tld.sima.rpgplugin.server.RaceInformation;

public class MainMenuEvents {
	private static Main plugin = Main.getPlugin(Main.class);
	
	public static void ItemParser(ItemStack item, Player player) {
		String name = item.getItemMeta().getDisplayName();
		// Create a new character
		if(name.contains("Create New Character")) {
			if(plugin.getRace().values() == null || plugin.getRace().values().size() == 0) {
				player.sendMessage(ChatColor.RED + "Unable to find any races! Ask an admin to create one");
				return;
			}
			Collection<RaceInformation> races = plugin.getRace().values();
			RaceInformation race = null;
			for(RaceInformation r : races) {
				if(r.getFlag()) {
					race = r;
					break;
				}
			}
			if(race == null) { 
				player.sendMessage(ChatColor.RED + "Unable to find a default race! Ask an admin to create one");
				return;
			}
			ItemStack[] wearables = new ItemStack[5];
			wearables[0] = new ItemStack(Material.AIR);
			wearables[1] = new ItemStack(Material.AIR);
			wearables[2] = new ItemStack(Material.AIR);
			wearables[3] = new ItemStack(Material.AIR);
			wearables[4] = new ItemStack(Material.AIR);
			CharacterInfo info = new CharacterInfo(	-1, -1, -1, -1, -1, -1,
													0, 0, 0, 0, 0, 0,
													wearables, 0, 0, player.getDisplayName(), "", "", "human", "",
													20, 20, 0, new ArrayList<String>(), new ArrayList<Buff>(), player.getLocation(), false);
			plugin.getCharacterList().get(player.getUniqueId()).addToList(info);
			player.sendMessage(ChatColor.GOLD + "New character created!");
			MainCharacterMenu i = new MainCharacterMenu();
			i.newInventory(player.getUniqueId(), player.getUniqueId(), 0);
		// Delete selected character
		}else if (name.contains("Delete")) {
			String[] tokens = name.split("[ ]");
			int value = Integer.parseInt(tokens[1]);
			plugin.getCharacterList().get(player.getUniqueId()).removeFromList(value);
			MainCharacterMenu i = new MainCharacterMenu();
			i.newInventory(player.getUniqueId(), player.getUniqueId(), 0);
		// Open menu of selected character
		}else if (name.contains("Character")) {
			String[] tokens = name.split("[ ]");
			int value = Integer.parseInt(tokens[1]);
			CharacterInfoMenu i = new CharacterInfoMenu();
			i.newInventory(player.getUniqueId(), plugin.getCharacterList().get(player.getUniqueId()).getList().get(value));
			plugin.getInventoryEventInfo().get(player.getUniqueId()).currentlylookedatcharacter = value;
		}
	}
}
