package tld.sima.rpgplugin.events;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryMenuInsertionEvents implements Listener {
	
	// Insertion to the event trees that parse the items
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		if (player == null) {
			return;
		}
		Inventory open = event.getClickedInventory();
		if (open == null) {
			return;
		}
		String mainMenuPrefix = ChatColor.DARK_BLUE + "RPG Main Character Menu";
		String characterMenuPrefix = ChatColor.DARK_BLUE + "RPG Character Menu";
		String raceMenuPrefix = ChatColor.DARK_BLUE + "RPG Race Menu";
		// Main RPG menu
		if (open.getName().equals(mainMenuPrefix)) {
			ItemStack item = event.getCurrentItem();
			if (item == null) {
//				player.sendMessage(ChatColor.WHITE + "Clicked something null!");
				return;
			}else if (!item.hasItemMeta() || !item.getItemMeta().hasDisplayName()) {
//				player.sendMessage(ChatColor.WHITE + "Clicked something without meta!");
				return;
			}
			event.setCancelled(true);
			MainMenuEvents.ItemParser(item, player);
		// Character menu
		}else if (open.getName().equals(characterMenuPrefix)) {
			ItemStack item = event.getCurrentItem();
			if (item == null) {
//				player.sendMessage(ChatColor.WHITE + "Clicked something null!");
				return;
			}else if (!item.hasItemMeta() || !item.getItemMeta().hasDisplayName()) {
//				player.sendMessage(ChatColor.WHITE + "Clicked something without meta!");
				return;
			}
			event.setCancelled(true);
			CharacterMenuEvents.ItemParser(item, player);
		// Race menu information
		}else if (open.getName().contains(raceMenuPrefix)) {
			ItemStack item = event.getCurrentItem();
			if (item == null) {
//				player.sendMessage(ChatColor.WHITE + "Clicked something null!");
				return;
			}else if (!item.hasItemMeta() || !item.getItemMeta().hasDisplayName()) {
//				player.sendMessage(ChatColor.WHITE + "Clicked something without meta!");
				return;
			}
			event.setCancelled(true);
			if(player.hasPermission("RPG.admin.race")) {
				RaceAdminMenuEvent.ItemParser(item, player);
			}
		}
	}
}
