package tld.sima.rpgplugin.events;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import tld.sima.rpgplugin.Main;
import tld.sima.rpgplugin.files.CharacterSave;
import tld.sima.rpgplugin.player.CharacterInfo;
import tld.sima.rpgplugin.player.CharacterList;
import tld.sima.rpgplugin.player.InventoryEventInformation;

public class GeneralEvents implements Listener {
	
	Main plugin = Main.getPlugin(Main.class);
	// Setup player character information to be easily accessible on the server, along with inventory stuff.
	@EventHandler
	public void onLogin(PlayerLoginEvent event) {
		Player player = event.getPlayer();
		plugin.getInventoryEventInfo().put(player.getUniqueId(), new InventoryEventInformation());
		String fileName = plugin.getDataFolder().toString() + File.separator + "Storage" + File.separator + "Characters" + File.separator + player.getUniqueId().toString();
		File check = new File(fileName);
		CharacterList list = new CharacterList(new ArrayList<CharacterInfo>());
		if(check.exists()) {
			File[] files = check.listFiles();
			for(File file : files) {
				CharacterSave save = new CharacterSave(file);
				CharacterInfo info = save.load();
				list.addToList(info);
			}
		}
		plugin.getCharacterList().put(player.getUniqueId(), list);
	}
	
	// Save all stats!
	@EventHandler
	public void onLogout(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		UUID uuid = player.getUniqueId();
		CharacterList list = plugin.getCharacterList().get(uuid);
		for(CharacterInfo info : list.getList()) {
			CharacterSave save = new CharacterSave(list.getList().indexOf(info) + "", uuid);
			save.save(info);
		}
	}
}
