package tld.sima.rpgplugin.events;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.conversations.Conversation;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import tld.sima.rpgplugin.Main;
import tld.sima.rpgplugin.conversations.CharacterNameConversation;
import tld.sima.rpgplugin.inventories.CharacterInfoMenu;
import tld.sima.rpgplugin.inventories.MainCharacterMenu;
import tld.sima.rpgplugin.player.CharacterInfo;

public class CharacterMenuEvents {

	private static Main plugin = Main.getPlugin(Main.class);
	
	public static void ItemParser(ItemStack item, Player player) {
		String name = item.getItemMeta().getDisplayName();
		UUID uuid = player.getUniqueId();
		// Setup stats to roll on a character sheet
		if(name.contains("Roll")) {
			CharacterInfo infoSelected = plugin.getCharacterList().get(uuid).getList().get(plugin.getInventoryEventInfo().get(uuid).currentlylookedatcharacter);
			int random = (Math.abs(plugin.getRandom().nextInt())%20) + 1;
			if(name.contains("Strength")) {
				infoSelected.setBaseStrength(random);
			}else if(name.contains("Dexterity")) {
				infoSelected.setBaseDexterity(random);
			}else if(name.contains("Constitution")) {
				infoSelected.setBaseConstitution(random);
			}else if(name.contains("Intelligence")) {
				infoSelected.setBaseIntelligence(random);
			}else if(name.contains("Wisdom")) {
				infoSelected.setBaseWisdom(random);
			}else if(name.contains("Charisma")) {
				infoSelected.setBaseCharisma(random);
			}else {
				return;
			}
			
			CharacterInfoMenu i = new CharacterInfoMenu();
			i.newInventory(uuid, infoSelected);
		// Roll stats through character sheet
		}else if (name.contains("Strength")){
			Bukkit.getServer().dispatchCommand(player, "roll s");
		}else if (name.contains("Dexterity")){
			Bukkit.getServer().dispatchCommand(player, "roll d");
		}else if (name.contains("Constitution")){
			Bukkit.getServer().dispatchCommand(player, "roll con");
		}else if (name.contains("Intelligence")){
			Bukkit.getServer().dispatchCommand(player, "roll i");
		}else if (name.contains("Wisdom")){
			Bukkit.getServer().dispatchCommand(player, "roll w");
		}else if (name.contains("Charisma")){
			Bukkit.getServer().dispatchCommand(player, "roll cha");
		// Change name
		}else if (name.contains("Name")) {
			player.closeInventory();
			ConversationFactory cf = new ConversationFactory(plugin);
			CharacterNameConversation conversation = new CharacterNameConversation();
			conversation.setData(uuid);
			Conversation conv = cf.withFirstPrompt(conversation).withLocalEcho(true).buildConversation(player);
			conv.begin();
			if(plugin.conversations.containsKey(uuid)) {
				plugin.conversations.get(uuid).abandon();
			}
			plugin.conversations.put(uuid, conv);
		// Change title
		}else if (name.contains("Title")) {
			// Title menu, titles need to be given to a player through someone with a greater title (or admin)
		// Set this character as currently selected character
		}else if (name.contains("Select Character")) {
			CharacterInfo infoSelected = plugin.getCharacterList().get(uuid).getList().get(plugin.getInventoryEventInfo().get(uuid).currentlylookedatcharacter);
			plugin.getCharacterList().get(uuid).changeSelectedCharacter(infoSelected);
			
			CharacterInfoMenu i = new CharacterInfoMenu();
			i.newInventory(uuid, infoSelected);
		// Return to main menu
		}else if (name.contains("Back")) {
			MainCharacterMenu i = new MainCharacterMenu();
			i.newInventory(player.getUniqueId(), player.getUniqueId(), 0);
		}
	}
}
