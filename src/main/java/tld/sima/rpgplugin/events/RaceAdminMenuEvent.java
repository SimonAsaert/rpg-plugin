package tld.sima.rpgplugin.events;

import java.util.UUID;

import org.bukkit.conversations.Conversation;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import tld.sima.rpgplugin.Main;
import tld.sima.rpgplugin.conversations.RaceNameConversation;
import tld.sima.rpgplugin.conversations.RaceStatChangeConversation;
import tld.sima.rpgplugin.inventories.RaceInformationMenu;

public class RaceAdminMenuEvent {
	private static Main plugin = Main.getPlugin(Main.class);
	
	public static void ItemParser(ItemStack item, Player player) {
		String name = item.getItemMeta().getDisplayName();
		UUID uuid = player.getUniqueId();
		// Change race name
		if(name.contains("Race")) {
			player.closeInventory();
			ConversationFactory cf = new ConversationFactory(plugin);
			RaceNameConversation conversation = new RaceNameConversation();
			conversation.setData(player.getUniqueId());
			Conversation conv = cf.withFirstPrompt(conversation).withLocalEcho(true).buildConversation(player);
			conv.begin();
			if(plugin.conversations.containsKey(uuid)) {
				plugin.conversations.get(uuid).abandon();
			}
			plugin.conversations.put(uuid, conv);
		// Toggle if this race is the selected race
		}else if (name.contains("Toggle")) {
			plugin.getAdminMap().get(uuid).raceinfo.setFlag(!plugin.getAdminMap().get(uuid).raceinfo.getFlag());
			RaceInformationMenu rim = new RaceInformationMenu();
			rim.newInventory(uuid, plugin.getAdminMap().get(uuid).raceinfo);
			return;
		// Exit menu
		}else if(name.contains("Back")) {
			player.closeInventory();
		// Set numerical stat of specific thing
		}else if (	name.contains("Strength") || name.contains("Dexterity") || name.contains("Constitution") ||  
				  	name.contains("Intelligence") || name.contains("Wisdom") || name.contains("Charisma")) {
			RaceStatChangeConversation conversation = new RaceStatChangeConversation();
			if(name.contains("Strength")) {
				conversation.setData(uuid, "Str");
			}else if (name.contains("Dex")) {
				conversation.setData(uuid, "Dex");
			}else if (name.contains("Con")) {
				conversation.setData(uuid, "Con");
			}else if (name.contains("Int")) {
				conversation.setData(uuid, "Int");
			}else if (name.contains("Wis")) {
				conversation.setData(uuid, "Wis");
			}else if (name.contains("Cha")) {
				conversation.setData(uuid, "Cha");
			}else {
				return;
			}
			player.closeInventory();
			ConversationFactory cf = new ConversationFactory(plugin);
			Conversation conv = cf.withFirstPrompt(conversation).withLocalEcho(true).buildConversation(player);
			conv.begin();
			if(plugin.conversations.containsKey(uuid)) {
				plugin.conversations.get(uuid).abandon();
			}
			plugin.conversations.put(uuid, conv);
		}
	}
}
