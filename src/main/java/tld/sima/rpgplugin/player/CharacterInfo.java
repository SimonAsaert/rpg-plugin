package tld.sima.rpgplugin.player;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

import tld.sima.rpgplugin.server.Buff;

public class CharacterInfo {
	// Currently selected flag
	private boolean flag = false;
	// Base character stats
	private int baseStrength;
	private int baseDexterity;
	private int baseConstitution;
	private int baseIntelligence;
	private int baseWisdom;
	private int baseCharisma;
	// Character stats from leveling up.
	private int extraStrength;
	private int extraDexterity;
	private int extraConstitution;
	private int extraIntelligence;
	private int extraWisdom;
	private int extraCharisma;
	// Health information
	private int maxhealth;
	private int health;
	private int temporaryhealth;
	// Server-based details
	private Location loc;
	// Items worn by player
	private ItemStack[] wearables;
	// Level information
	private int level;
	private int xp;
	// Public information
	private String name;
	private String title;
	private String description;
	private String race;
	// Private background information
	private String background;
	// Feats and buffs
	private ArrayList<String> features;
	private ArrayList<Buff> buffs;
	
	public CharacterInfo(int baseStrength, int baseDexterity, int baseConstitution, int baseIntelligence, int baseWisdom, int baseCharisma,
			int extraStrength, int extraDexterity, int extraConstitution, int extraIntelligence, int extraWisdom, int extraCharisma, 
			ItemStack[] wearables, int level, int xp, String name, String title, String description, String race, String background,
			int maxhealth, int health, int temporaryhealth, List<String> features, List<Buff> buffs, Location loc, boolean flag) {
		this.baseStrength = baseStrength;
		this.baseDexterity = baseDexterity;
		this.baseConstitution = baseConstitution;
		this.baseIntelligence = baseIntelligence;
		this.baseWisdom = baseWisdom;
		this.baseCharisma = baseCharisma;
		
		this.extraStrength = extraStrength;
		this.extraDexterity = extraDexterity;
		this.extraConstitution = extraConstitution;
		this.extraIntelligence = extraIntelligence;
		this.extraWisdom = extraWisdom;
		this.extraCharisma = extraCharisma;

		this.maxhealth = maxhealth;
		this.health = health;
		this.temporaryhealth = temporaryhealth;
		
		this.wearables = wearables;
		this.level = level;
		this.xp = xp;
		this.name = name;
		this.title = title;
		this.description = description;
		this.race = race;
		this.background = background;
		
		this.features = (ArrayList<String>) features;
		this.buffs = (ArrayList<Buff>) buffs;
		this.loc = loc;
		
		this.flag = flag;
	}
	
	public int getBaseStrength() {			return baseStrength;}
	public int getBaseDexterity() {			return baseDexterity;}
	public int getBaseConstitution() { 		return baseConstitution;}
	public int getBaseIntelligence() { 		return baseIntelligence;}
	public int getBaseWisdom() {			return baseWisdom;}
	public int getBaseCharisma() {			return baseCharisma;}
	
	public int getExtraStrength() {			return extraStrength;}
	public int getExtraDexterity() {		return extraDexterity;}
	public int getExtraConstitution() {		return extraConstitution;}
	public int getExtraIntelligence() {		return extraIntelligence;}
	public int getExtraWisdom() {			return extraWisdom;}
	public int getExtraCharisma() {			return extraCharisma;}
	
	public int getMaxHealth() {				return maxhealth;}
	public int getHealth() {				return health;}
	public int getTemporaryHealth() {		return temporaryhealth;}
	
	public Location getLocation() {			return loc;}
	
	public ItemStack[] getWearables() {		return wearables;}
	public int getLevel() { 				return level;}
	public int getExperience() {			return xp;}
	
	public String getName() {				return name;}
	public String getTitle() {				return title;}
	public String getDescription() {		return description;}
	public String getRace() {				return race;}
	public String getBackground() {			return background;}
	
	public ArrayList<String> getFeatures(){	return features;}
	public ArrayList<Buff> getBuffs(){		return buffs;}
	
	public boolean getFlag() {				return flag;}
	
	public void setBaseStrength(int value) {			this.baseStrength = value;}
	public void setBaseDexterity(int value) {			this.baseDexterity = value;}
	public void setBaseConstitution(int value) {		this.baseConstitution = value;}
	public void setBaseIntelligence(int value) {		this.baseIntelligence = value;}
	public void setBaseWisdom(int value) {				this.baseWisdom = value;}
	public void setBaseCharisma(int value) {			this.baseCharisma = value;}
	
	public void setExtraStrength(int value) {			this.extraStrength = value;}
	public void setExtraDexterity(int value) { 			this.extraDexterity = value;}
	public void setExtraConstitution(int value) {		this.extraConstitution = value;}
	public void setExtraIntelligence(int value) {		this.extraIntelligence = value;}
	public void setExtraWisdom(int value) {				this.extraWisdom = value;}
	public void setExtraCharisma(int value) {			this.baseCharisma = value;}
	
	public void setMaxHealth(int value) {				this.maxhealth = value;}
	public void setHealth(int value) { 					this.health = value;}
	public void setTemporaryHealth(int value) {			this.temporaryhealth = value;}
	
	public void setLocation(Location value) {			this.loc = value;}
	
	public void setWearables(ItemStack[] value) {		this.wearables = value;}
	
	public void setLevel(int value) {					this.level = value;}
	public void setExperience(int value) {				this.xp = value;}
	
	public void setName(String value) {					this.name = value;}
	public void setTitle(String value) {				this.title = value;}
	public void setDescription(String value) {			this.description = value;}
	public void setRace(String value) {					this.race = value;}
	public void setBackground(String value) {			this.background = value;}
	
	public void setFeatures(ArrayList<String> value) {	this.features = value;}
	public void setBuffs(ArrayList<Buff> value) {		this.buffs = value;}
	
	public void setFlag(boolean value) {				this.flag = value;}
}
