package tld.sima.rpgplugin.player;

import org.bukkit.inventory.ItemStack;

public class PlayerInventory {
	
	private ItemStack[] items;
	private ItemStack[] armor;
	private ItemStack[] enderchest;
	
	public PlayerInventory(ItemStack[] items, ItemStack[] armor, ItemStack[] enderchest) {
		this.items = items;
		this.armor = armor;
		this.enderchest = enderchest;
	}
	
	public PlayerInventory() {
		
	}
	
	public ItemStack[] getItems() {
		return this.items;
	}
	
	public ItemStack[] getArmor() {
		return this.armor;
	}
	
	public ItemStack[] getEnderchest() {
		return this.enderchest;
	}
}
