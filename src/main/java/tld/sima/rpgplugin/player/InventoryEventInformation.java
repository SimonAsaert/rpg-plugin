package tld.sima.rpgplugin.player;

public class InventoryEventInformation {
	// What is the left-most character in the displayed menu
	public int insertionpoint;
	// Character that was last looked at in menu
	public int currentlylookedatcharacter;
	
	public InventoryEventInformation() {
		insertionpoint = 0;
		currentlylookedatcharacter = 0;
	}
	
	public InventoryEventInformation(int a, int b) {
		insertionpoint = a;
		currentlylookedatcharacter = b;
	}
}
