package tld.sima.rpgplugin.player;

import java.util.ArrayList;

public class CharacterList {
	// Selected character currently
	public int selected;
	// List of characters assigned to players
	public ArrayList<CharacterInfo> list;
	
	public CharacterList(ArrayList<CharacterInfo> characters) {
		selected = -1;
		list = characters;
		for(CharacterInfo info : characters) {
			if(info.getFlag()) {
				selected = list.indexOf(info);
				break;
			}
		}
	}
	
	public CharacterInfo getSelectedCharacter() {
		if(selected == -1) {
			return null;
		}
		return list.get(selected);
	}
	
	public ArrayList<CharacterInfo> getList(){
		return list;
	}
	
	public void changeSelectedCharacter(int i) {
		selected = i;
	}
	
	public boolean changeSelectedCharacter(CharacterInfo selected) {
		if(this.selected != -1) {
			list.get(this.selected).setFlag(false);
		}
		this.selected = list.indexOf(selected);
		list.get(this.selected).setFlag(true);
		
		return true;
	}
	
	public void addToList(CharacterInfo character) {
		list.add(character);
		if(character.getFlag()) {
			changeSelectedCharacter(character);
		}
	}
	
	public void removeFromList(CharacterInfo character) {
		list.remove(character);
	}
	
	public void removeFromList(int index) {
		list.remove(index);
	}
}
