package tld.sima.rpgplugin.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import tld.sima.rpgplugin.Main;
import tld.sima.rpgplugin.inventories.MainCharacterMenu;

public class CharacterCommands implements CommandExecutor {

	Main plugin = Main.getPlugin(Main.class);
	
	public String cmd1 = "Char";
	public String cmd2 = "Character";
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		if(sender instanceof Player) {
			Player player = (Player) sender;
			// Char, open character menu
			if(command.getName().equalsIgnoreCase(cmd1) || command.getName().equalsIgnoreCase(cmd2)) {
				if (args.length == 0 || !player.hasPermission("rpg.character.view.other")) {
					MainCharacterMenu i = new MainCharacterMenu();
					i.newInventory(player.getUniqueId(), player.getUniqueId(), 0);
					return true;
				}else if (args[1].equalsIgnoreCase("admin") && player.hasPermission("rpg.character.view.admin")) {
					
				}else {
					Player otherPlayer = Bukkit.getPlayer(args[0]);
					if(otherPlayer.equals(null)) {
						player.sendMessage(ChatColor.RED + "Unable to find other player!");
						return false;
					}
					MainCharacterMenu i = new MainCharacterMenu();
					i.newInventory(player.getUniqueId(), otherPlayer.getUniqueId(), 0);
					return true;
				}
			}
		}
		
		return false;
	}
	

}
