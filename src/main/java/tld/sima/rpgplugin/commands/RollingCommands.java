package tld.sima.rpgplugin.commands;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import tld.sima.rpgplugin.Main;
import tld.sima.rpgplugin.player.CharacterInfo;
import tld.sima.rpgplugin.server.Rolling;

public class RollingCommands implements CommandExecutor {

	static Main plugin = Main.getPlugin(Main.class);
	
	public String cmd1 = "Roll";

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			// ROLLING EVERYTHING!
			if(command.getName().equalsIgnoreCase(cmd1)) {
				// Default roll 1d20
				if (args.length == 0) {
					roll(player.getName(), 1, 20);
					return true;
				// If stat rolling only has 1 thing, allows me to display criticals at the end
				}else if (args.length == 1) {
					if (args[0].equalsIgnoreCase("dex") || args[0].equalsIgnoreCase("d") || args[0].equalsIgnoreCase("Dexterity")) {
						UUID playerUUID = player.getUniqueId();
						if(plugin.getCharacterList().get(playerUUID) == null) {
							player.sendMessage(ChatColor.RED + "You have to select a character first!");
							return false;
						}
						CharacterInfo selected = plugin.getCharacterList().get(playerUUID).getSelectedCharacter();
						if(selected == null) {
							player.sendMessage(ChatColor.RED + "You have to select a character first!");
							return false;
						}
						if (selected.getBaseDexterity() == -1) {
							player.sendMessage(ChatColor.RED + "You have to roll your character stats first!");
							return false;
						}
						rollStat(selected.getBaseDexterity(), selected.getExtraDexterity(), plugin.getRace().get(selected.getRace()).getDexterity(), "Dexterity", player.getName());
						return true;
					}else if (args[0].equalsIgnoreCase("str") || args[0].equalsIgnoreCase("s") || args[0].equalsIgnoreCase("strength")) {
						UUID playerUUID = player.getUniqueId();
						if(plugin.getCharacterList().get(playerUUID).equals(null)) {
							player.sendMessage(ChatColor.RED + "You have to select a character first!");
							return false;
						}
						CharacterInfo selected = plugin.getCharacterList().get(playerUUID).getSelectedCharacter();
						if(selected == null) {
							player.sendMessage(ChatColor.RED + "You have to select a character first!");
							return false;
						}
						if (selected.getBaseDexterity() == -1) {
							player.sendMessage(ChatColor.RED + "You have to roll your character stats first!");
							return false;
						}
						rollStat(selected.getBaseStrength(), selected.getExtraStrength(), plugin.getRace().get(selected.getRace()).getStrength(), "Strength", player.getName());
						return true;
					}else if (args[0].equalsIgnoreCase("Con")|| args[0].equalsIgnoreCase("Constitution")) {
						UUID playerUUID = player.getUniqueId();
						if(plugin.getCharacterList().get(playerUUID) == null ) {
							player.sendMessage(ChatColor.RED + "You have to select a character first!");
							return false;
						}
						CharacterInfo selected = plugin.getCharacterList().get(playerUUID).getSelectedCharacter();
						if(selected == null) {
							player.sendMessage(ChatColor.RED + "You have to select a character first!");
							return false;
						}
						if (selected.getBaseDexterity() == -1) {
							player.sendMessage(ChatColor.RED + "You have to roll your character stats first!");
							return false;
						}
						rollStat(selected.getBaseConstitution(), selected.getExtraConstitution(), plugin.getRace().get(selected.getRace()).getConstitution(), "Constitution", player.getName());
						return true;
					}else if (args[0].equalsIgnoreCase("Int") || args[0].equalsIgnoreCase("I") || args[0].equalsIgnoreCase("Intelligence")) {
						UUID playerUUID = player.getUniqueId();
						if(plugin.getCharacterList().get(playerUUID) == null) {
							player.sendMessage(ChatColor.RED + "You have to select a character first!");
							return false;
						}
						CharacterInfo selected = plugin.getCharacterList().get(playerUUID).getSelectedCharacter();
						if(selected == null ) {
							player.sendMessage(ChatColor.RED + "You have to select a character first!");
							return false;
						}
						if (selected.getBaseDexterity() == -1) {
							player.sendMessage(ChatColor.RED + "You have to roll your character stats first!");
							return false;
						}
						rollStat(selected.getBaseIntelligence(), selected.getExtraIntelligence(), plugin.getRace().get(selected.getRace()).getIntelligence(), "Intelligence", player.getName());
						return true;
					}else if (args[0].equalsIgnoreCase("wis") || args[0].equalsIgnoreCase("w") || args[0].equalsIgnoreCase("Wisdom")) {
						UUID playerUUID = player.getUniqueId();
						if(plugin.getCharacterList().get(playerUUID) == null) {
							player.sendMessage(ChatColor.RED + "You have to select a character first!");
							return false;
						}
						CharacterInfo selected = plugin.getCharacterList().get(playerUUID).getSelectedCharacter();
						if(selected == null) {
							player.sendMessage(ChatColor.RED + "You have to select a character first!");
							return false;
						}
						if (selected.getBaseDexterity() == -1) {
							player.sendMessage(ChatColor.RED + "You have to roll your character stats first!");
							return false;
						}
						rollStat(selected.getBaseWisdom(), selected.getExtraWisdom(), plugin.getRace().get(selected.getRace()).getWisdom(), "Wisdom", player.getName());
						return true;
					}else if (args[0].equalsIgnoreCase("Cha")|| args[0].equalsIgnoreCase("Charisma")) {
						UUID playerUUID = player.getUniqueId();
						if(plugin.getCharacterList().get(playerUUID) == null) {
							player.sendMessage(ChatColor.RED + "You have to select a character first!");
							return false;
						}
						CharacterInfo selected = plugin.getCharacterList().get(playerUUID).getSelectedCharacter();
						if(selected == null) {
							player.sendMessage(ChatColor.RED + "You have to select a character first!");
							return false;
						}
						if (selected.getBaseDexterity() == -1) {
							player.sendMessage(ChatColor.RED + "You have to roll your character stats first!");
							return false;
						}
						rollStat(selected.getBaseCharisma(), selected.getExtraCharisma(), plugin.getRace().get(selected.getRace()).getCharisma(), "Charisma", player.getName());
						return true;
					// Parse "#d@" where # = number of dice, @ = number of sides on each dice.
					}else if (args[0].contains("d") || args[0].contains("D")) {
						String[] tokens;
						if (args[0].contains("d")) {
							tokens = args[0].split("[d]");
						}else if (args[0].contains("D")) {
							tokens = args[0].split("[D]");
						}else {
							return false;
						}
						if(tokens.length != 2) {
							return false;
						}
						int numdice;
						try {
							numdice = Integer.parseInt(tokens[0]);
						}catch(NumberFormatException e) {
							numdice = 1;
						}
						int sides;
						try {
							sides = Integer.parseInt(tokens[1]);
						}catch(NumberFormatException e) {
							sides = 1;
						}
						roll(player.getName(), numdice, sides);
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private void rollStat(int base, int extra, int race, String type, String name) {
		int normalised = (base + extra + race - 10)/2;
		int randomValue = (Math.abs(plugin.getRandom().nextInt())%20)+1;
		int finalValue = normalised + randomValue;
		StringBuilder message = new StringBuilder();
		message.append(ChatColor.WHITE + name + ChatColor.GOLD + " rolled " + type + ": ");
		message.append(ChatColor.WHITE).append((base-10)/2).append(ChatColor.GOLD).append(" + ");
		message.append(ChatColor.WHITE).append(extra/2).append(ChatColor.GOLD).append(" + ");
		message.append(ChatColor.WHITE).append(race/2).append(ChatColor.GOLD).append(" + ");
		
		message.append(checkValue(randomValue, 20)).append(randomValue);
		message.append(ChatColor.WHITE).append(" = ").append(ChatColor.GOLD).append(checkValue(finalValue, 20 + normalised));
		Bukkit.broadcastMessage(message.toString());
	}
	
	private void roll(String name, int dicenumber, int sides) {
		ArrayList<Integer> values = Rolling.roll(dicenumber, sides);
		StringBuilder message = new StringBuilder();
		message.append(ChatColor.WHITE).append(name).append(ChatColor.GOLD).append(" rolled ").append(ChatColor.WHITE).append(dicenumber).append("d").append(sides);
		message.append(ChatColor.GOLD).append(" : ").append(checkValue(values.get(0), sides)).append(values.get(0));
		for(int i = 1 ; i < (values.size() - 1) ; i++) {
			message.append(ChatColor.GOLD).append(" + ").append(checkValue(values.get(i), sides)).append(values.get(i));
		}
		message.append(ChatColor.GOLD).append(" = ").append(values.get(values.size() - 1));
		Bukkit.broadcastMessage(message.toString());
	}
	
	private ChatColor checkValue(int i, int max) {
		if(i == max) {
			return ChatColor.GREEN;
		}else if (i == 1) {
			return ChatColor.RED;
		}else {
			return ChatColor.WHITE;
		}
	}
}
