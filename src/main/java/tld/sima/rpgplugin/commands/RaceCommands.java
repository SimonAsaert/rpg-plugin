package tld.sima.rpgplugin.commands;

import java.util.ArrayList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Conversation;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;

import net.md_5.bungee.api.ChatColor;
import tld.sima.rpgplugin.Main;
import tld.sima.rpgplugin.conversations.RaceNameConversation;
import tld.sima.rpgplugin.inventories.RaceInformationMenu;
import tld.sima.rpgplugin.player.AdminSetup;
import tld.sima.rpgplugin.server.RaceInformation;

public class RaceCommands implements CommandExecutor{
	
	public String cmd1 = "createrace";
	public String cmd2 = "editrace";
	private Main plugin = Main.getPlugin(Main.class);

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			// Createrace, creates a new race
			if(command.getName().equalsIgnoreCase(cmd1)) {
				RaceInformation info = new RaceInformation();
				plugin.getAdminMap().get(player.getUniqueId()).raceinfo = info;
				ConversationFactory cf = new ConversationFactory(plugin);
				RaceNameConversation conversation = new RaceNameConversation();
				conversation.setData(player.getUniqueId());
				Conversation conv = cf.withFirstPrompt(conversation).withLocalEcho(true).buildConversation(player);
				conv.begin();
				if(plugin.conversations.get(player.getUniqueId()) != null) {
					plugin.conversations.get(player.getUniqueId()).abandon();
				}
				plugin.conversations.put(player.getUniqueId(), conv);
			// Editrace, edit an existing race, or see the current list of races
			}else if (command.getName().equalsIgnoreCase(cmd2)) {
				if(!plugin.getAdminMap().containsKey(player.getUniqueId())) {
					plugin.getAdminMap().put(player.getUniqueId(), new AdminSetup());
				}
				if(args.length == 0) {
					if(!plugin.getAdminMap().containsKey(player.getUniqueId()) || (plugin.getAdminMap().get(player.getUniqueId()).raceinfo == null)) {
						if(!plugin.getAdminMap().containsKey(player.getUniqueId())) {
							AdminSetup setup = new AdminSetup();
							plugin.getAdminMap().put(player.getUniqueId(), setup);
						}
						player.sendMessage(ChatColor.RED + "You need to select a race first!");
						StringBuilder builder = new StringBuilder();
						ArrayList<String> names = Lists.newArrayList( plugin.getRace().keySet().iterator());
						if(names.size() > 0) {
							builder.append(names.get(0));
							for(int i = 1 ; i < names.size() ; i++) {
								builder.append(", ").append(names.get(i));
							}
							player.sendMessage(builder.toString());
						}
						return false;
					}
					RaceInformationMenu menu = new RaceInformationMenu();
					menu.newInventory(player.getUniqueId(), plugin.getAdminMap().get(player.getUniqueId()).raceinfo);
					
				}else{
					if(!plugin.getRace().containsKey(args[0].toLowerCase())) {
						player.sendMessage(ChatColor.RED + "Unable to find race!");
						StringBuilder builder = new StringBuilder();
						String[] names = (String[])plugin.getRace().keySet().toArray();
						if(names.length > 0) {
							builder.append(names[0]);
							for(int i = 1 ; i < names.length ; i++) {
								builder.append(", ").append(names[i]);
							}
							player.sendMessage(builder.toString());
						}
						return false;
					}
					RaceInformationMenu menu = new RaceInformationMenu();
					menu.newInventory(player.getUniqueId(), plugin.getRace().get(args[0].toLowerCase()));
					plugin.getAdminMap().get(player.getUniqueId()).raceinfo = plugin.getRace().get(args[0].toLowerCase());
					return true;
				}
			}
		}
		return false;
	}

}
