package tld.sima.rpgplugin.files;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import tld.sima.rpgplugin.Main;
import tld.sima.rpgplugin.server.RaceInformation;

public class RaceSave {
	
	Main plugin = Main.getPlugin(Main.class);
	
	File file;
	FileConfiguration fileconfig;
	public RaceSave(String fileName) {
		if(!plugin.getDataFolder().exists()) {
			plugin.getDataFolder().mkdir();
		}
		
		String directory = plugin.getDataFolder().toString() + File.separator + "Storage";
		File tmp = new File(directory);
		if(!tmp.exists()) {
			tmp.mkdir();
		}
		directory += File.separator + "Races";
		tmp = new File(directory);
		if(!tmp.exists()) {
			tmp.mkdir();
		}
		directory += File.separator + fileName + ".yml";
		tmp = new File(directory);
		if (!tmp.exists()) {
			try {
				tmp.createNewFile();
			}catch(IOException e) {
				e.printStackTrace();
				Bukkit.getServer().getConsoleSender().sendMessage(net.md_5.bungee.api.ChatColor.RED + "Storage file unable to be created!");
			}
		}
		file = tmp;
		fileconfig = YamlConfiguration.loadConfiguration(file);
		createStorageValues();
	}
	
	public RaceSave(File file) {
		this.file = file;
		fileconfig = YamlConfiguration.loadConfiguration(this.file);
		createStorageValues();
	}

	private void createStorageValues() {
		fileconfig.addDefault("Race.default", false);
		fileconfig.addDefault("Race.name", "");
		fileconfig.addDefault("Race.stats.strength", 0);
		fileconfig.addDefault("Race.stats.dexterity", 0);
		fileconfig.addDefault("Race.stats.constitution", 0);
		fileconfig.addDefault("Race.stats.intelligence", 0);
		fileconfig.addDefault("Race.stats.wisdom", 0);
		fileconfig.addDefault("Race.stats.charisma", 0);
		fileconfig.addDefault("Race.stats.features", new ArrayList<String>());
		fileconfig.options().copyDefaults(true);
		savecfg();
	}

	private boolean savecfg() {
		try {
			fileconfig.save(file);
		} catch (IOException e) {
			Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "Unable to save storage file!" );
			return false;
		}
		return true;
	}

	public void setupRace(RaceInformation ri) {
		fileconfig.set("Race.default", ri.getFlag());
		fileconfig.set("Race.name", ri.getName());
		fileconfig.set("Race.stats.strength", ri.getStrength());
		fileconfig.set("Race.stats.dexterity", ri.getDexterity());
		fileconfig.set("Race.stats.constitution", ri.getConstitution());
		fileconfig.set("Race.stats.intelligence", ri.getIntelligence());
		fileconfig.set("Race.stats.wisdom", ri.getWisdom());
		fileconfig.set("Race.stats.charisma", ri.getCharisma());
		fileconfig.set("Race.stats.features", ri.getfeatures());
		savecfg();
	}
	
	public RaceInformation getRace() {
		String racename = fileconfig.getString("Race.name");
		
		int strength = fileconfig.getInt("Race.stats.strength");
		int dexterity = fileconfig.getInt("Race.stats.dexterity");
		int constitution = fileconfig.getInt("Race.stats.constitution");
		int intelligence = fileconfig.getInt("Race.stats.intelligence");
		int wisdom = fileconfig.getInt("Race.stats.wisdom");
		int charisma = fileconfig.getInt("Race.stats.charisma");
		List<String> features = fileconfig.getStringList("Race.stats.features");
		
		boolean flag = true;
		
		return new RaceInformation(racename, strength, dexterity, constitution, intelligence, wisdom, charisma, (ArrayList<String>)features, flag);
	}
	
	public int getStr() {
		return fileconfig.getInt("Race.stats.strength");
	}
	
	public int getDex() {
		return fileconfig.getInt("Race.stats.dexterity");
	}
	
	public int getCon() {
		return fileconfig.getInt("Race.stats.constitution");
	}
	
	public int getInt() {
		return fileconfig.getInt("Race.stats.intelligence");
	}
	
	public int getWis() {
		return fileconfig.getInt("Race.stats.wisdom");
	}
	
	public int getCha() {
		return fileconfig.getInt("Race.stats.charisma");
	}
	
	public List<String> getFeatures() {
		return fileconfig.getStringList("Race.stats.features");
	}
}
