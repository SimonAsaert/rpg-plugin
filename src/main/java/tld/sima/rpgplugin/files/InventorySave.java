package tld.sima.rpgplugin.files;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import net.md_5.bungee.api.ChatColor;
import tld.sima.rpgplugin.InventoryConverter;
import tld.sima.rpgplugin.Main;
import tld.sima.rpgplugin.player.PlayerInventory;

public class InventorySave {
	
	Main plugin = Main.getPlugin(Main.class);
	String filename;
	
	public InventorySave(UUID uuid, String fileName) {
		if(!plugin.getDataFolder().exists()) {
			plugin.getDataFolder().mkdir();
		}
		String directoryName = plugin.getDataFolder().toString() + File.separator + uuid.toString();
		File tmp = new File(directoryName);
		if(!tmp.exists()) {
			tmp.mkdir();
		}
		directoryName += File.separator + "Inventories";
		tmp = new File(directoryName);
		if(!tmp.exists()) {
			tmp.mkdir();
		}
		directoryName += File.separator + fileName + ".json";
		this.filename = directoryName;
	}
	
	public InventorySave(String directory) {
		this.filename = directory;
	}
	
	public boolean isCreated() {
		File file = new File(filename);
		return file.exists();
	}
	
	@SuppressWarnings("unchecked")
	public boolean saveData(ArrayList<String> items, ArrayList<String> armor, ArrayList<String> enderchest) {
		JSONObject obj = new JSONObject();

		JSONArray itema = new JSONArray();
		itema.addAll(items);
		
		JSONArray armora = new JSONArray();
		armora.addAll(armor);
		
		JSONArray enderchesta = new JSONArray();
		enderchesta.addAll(enderchest);
		
		obj.put("Items", itema);
		obj.put("Armor", armora);
		obj.put("Enderchest", enderchesta);
		
		try{
			FileWriter file = new FileWriter(filename);
			Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.GREEN + "File name: " + ChatColor.WHITE + filename);
			file.write(obj.toJSONString());
			file.flush();
			file.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public PlayerInventory getInventory() {
		PlayerInventory i = null;
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader(filename));
			JSONObject jsonObject = (JSONObject) obj;
			ArrayList<String> items = (ArrayList<String>) jsonObject.get("Items");
			ArrayList<String> armor = (ArrayList<String>) jsonObject.get("Armor");
			ArrayList<String> enderchest = (ArrayList<String>) jsonObject.get("Enderchest");
			
			ItemStack[] itemss = new ItemStack[items.size()];
			for(int j = 0 ; j < items.size(); j++) {
				itemss[j] = InventoryConverter.getItem(items.get(j));
			}
			ItemStack[] armors = new ItemStack[armor.size()];
			for(int j = 0 ; j < armor.size(); j++) {
				armors[j] = InventoryConverter.getItem(armor.get(j));
			}
			ItemStack[] enderchests = new ItemStack[enderchest.size()];
			for(int j = 0 ; j < enderchest.size(); j++) {
				enderchests[j] = InventoryConverter.getItem(enderchest.get(j));
			}

			i = new PlayerInventory(itemss, armors, enderchests);
		} catch (IOException e) {	
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (org.json.simple.parser.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return i;
	}

}
