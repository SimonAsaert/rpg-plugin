package tld.sima.rpgplugin.files;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import tld.sima.rpgplugin.InventoryConverter;
import tld.sima.rpgplugin.Main;
import tld.sima.rpgplugin.player.CharacterInfo;
import tld.sima.rpgplugin.server.Buff;

public class CharacterSave {

	Main plugin = Main.getPlugin(Main.class);
	
	File file;
	FileConfiguration fileconfig;
	public CharacterSave(String fileName, UUID uuid) {
		if(!plugin.getDataFolder().exists()) {
			plugin.getDataFolder().mkdir();
		}
		
		String directory = plugin.getDataFolder().toString() + File.separator + "Storage";
		File tmp = new File(directory);
		if(!tmp.exists()) {
			tmp.mkdir();
		}
		directory += File.separator + "Characters";
		tmp = new File(directory);
		if(!tmp.exists()) {
			tmp.mkdir();
		}
		directory += File.separator + uuid.toString();
		tmp = new File(directory);
		if(!tmp.exists()) {
			tmp.mkdir();
		}
		directory += File.separator + fileName + ".yml";
		tmp = new File(directory);
		if (!tmp.exists()) {
			try {
				tmp.createNewFile();
			}catch(IOException e) {
				e.printStackTrace();
				Bukkit.getServer().getConsoleSender().sendMessage(net.md_5.bungee.api.ChatColor.RED + "Storage file unable to be created!");
			}
		}
		file = tmp;
		fileconfig = YamlConfiguration.loadConfiguration(file);
		createStorageValues();
	}
	
	public CharacterSave(File file) {
		this.file = file;
		fileconfig= YamlConfiguration.loadConfiguration(this.file);
		createStorageValues();
	}

	private void createStorageValues() {
		fileconfig.addDefault("Character.stats.baseStrength", 0);
		fileconfig.addDefault("Character.stats.baseDexterity", 0);
		fileconfig.addDefault("Character.stats.baseConstitution", 0);
		fileconfig.addDefault("Character.stats.baseIntelligence", 0);
		fileconfig.addDefault("Character.stats.baseWisdom", 0);
		fileconfig.addDefault("Character.stats.baseCharisma", 0);
		
		fileconfig.addDefault("Character.stats.extraStrength", 0);
		fileconfig.addDefault("Character.stats.extraDexterity", 0);
		fileconfig.addDefault("Character.stats.extraConstitution", 0);
		fileconfig.addDefault("Character.stats.extraIntelligence", 0);
		fileconfig.addDefault("Character.stats.extraWisdom", 0);
		fileconfig.addDefault("Character.stats.extraCharisma", 0);

		fileconfig.addDefault("Character.stats.maxhealth", 0);
		fileconfig.addDefault("Character.stats.health", 0);
		fileconfig.addDefault("Character.stats.temporaryhealth", 0);
		
		fileconfig.addDefault("Character.location.World", "");
		fileconfig.addDefault("Character.location.x", 0);
		fileconfig.addDefault("Character.location.y", 0);
		fileconfig.addDefault("Character.location.z", 0);
		fileconfig.addDefault("Character.location.yaw", 0);
		fileconfig.addDefault("Character.location.pitch", 0);

		fileconfig.addDefault("Character.inventory.wearables", new ArrayList<String>());
		
		fileconfig.addDefault("Character.stats.level", 0);
		fileconfig.addDefault("Character.stats.experience", 0);
		
		fileconfig.addDefault("Character.description.name", "");
		fileconfig.addDefault("Character.description.title", "");
		fileconfig.addDefault("Character.description.description", "");
		fileconfig.addDefault("Character.description.race", "");
		
		fileconfig.addDefault("Character.description.background", "");
		
		fileconfig.addDefault("Character.stats.features", new ArrayList<String>());
		fileconfig.addDefault("Character.stats.buffs", new ArrayList<String>());
		
		fileconfig.addDefault("Character.flag", false);
		
		fileconfig.options().copyDefaults(true);
		savecfg();
	}

	private boolean savecfg() {
		try {
			fileconfig.save(file);
		} catch (IOException e) {
			Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "Unable to save storage file!" );
			return false;
		}
		return true;
	}
	
	public void save(CharacterInfo info) {
		fileconfig.set("Character.stats.baseStrength", info.getBaseStrength());
		fileconfig.set("Character.stats.baseDexterity", info.getBaseDexterity());
		fileconfig.set("Character.stats.baseConstitution", info.getBaseConstitution());
		fileconfig.set("Character.stats.baseIntelligence", info.getBaseIntelligence());
		fileconfig.set("Character.stats.baseWisdom", info.getBaseWisdom());
		fileconfig.set("Character.stats.baseCharisma", info.getBaseCharisma());
		
		fileconfig.set("Character.stats.extraStrength", info.getExtraStrength());
		fileconfig.set("Character.stats.extraDexterity", info.getExtraDexterity());
		fileconfig.set("Character.stats.extraConstitution", info.getExtraConstitution());
		fileconfig.set("Character.stats.extraIntelligence", info.getExtraIntelligence());
		fileconfig.set("Character.stats.extraWisdom", info.getExtraWisdom());
		fileconfig.set("Character.stats.extraCharisma", info.getExtraCharisma());
		
		fileconfig.set("Character.stats.maxhealth", info.getMaxHealth());
		fileconfig.set("character.stats.health", info.getHealth());
		fileconfig.set("Character.stats.temporaryhealth", info.getTemporaryHealth());
		
		Location loc = info.getLocation();
		
		fileconfig.set("Character.location.World", loc.getWorld().getUID().toString());
		fileconfig.set("Character.location.x", loc.getX());
		fileconfig.set("Character.location.y", loc.getY());
		fileconfig.set("Character.location.z", loc.getZ());
		fileconfig.set("Character.location.yaw", loc.getYaw());
		fileconfig.set("Character.location.pitch", loc.getPitch());
		
		fileconfig.set("Character.stats.level", info.getLevel());
		fileconfig.addDefault("Character.stats.experience", info.getExperience());
		
		fileconfig.set("Character.description.name", info.getName());
		fileconfig.set("Character.description.title", info.getTitle());
		fileconfig.set("Character.description.description", info.getDescription());
		fileconfig.set("Character.description.race", info.getRace());
		
		fileconfig.set("Character.description.background", info.getBackground());
		
		fileconfig.set("Character.stats.features", info.getFeatures());
		
		ArrayList<String> buffs = new ArrayList<String>();
		ArrayList<Buff> buff = info.getBuffs();
		for(Buff buf: buff) {
			buffs.add(buf.name());
		}
		fileconfig.set("Character.stats.buffs", buffs);
		
		ArrayList<String> wearablesS = new ArrayList<String>();
		ItemStack[] wearables = info.getWearables();
		for(ItemStack item : wearables) {
			wearablesS.add(InventoryConverter.getString(item));
		}
		fileconfig.set("Character.inventory.wearables", wearablesS);
		
		fileconfig.set("Character.flag", info.getFlag());
		savecfg();
	}

	public CharacterInfo load() {
		int baseStrength = fileconfig.getInt("Character.stats.baseStrength");
		int baseDexterity = fileconfig.getInt("Character.stats.baseDexterity");
		int baseConstitution = fileconfig.getInt("Character.stats.baseConstitution");
		int baseIntelligence = fileconfig.getInt("Character.stats.baseIntelligence");
		int baseWisdom = fileconfig.getInt("Character.stats.baseWisdom");
		int baseCharisma = fileconfig.getInt("Character.stats.baseCharisma");
		
		int extraStrength = fileconfig.getInt("Character.stats.extraStrength");
		int extraDexterity = fileconfig.getInt("Character.stats.extraDexterity");
		int extraConstitution = fileconfig.getInt("Character.stats.extraConstitution");
		int extraIntelligence = fileconfig.getInt("Character.stats.extraIntelligence");
		int extraWisdom = fileconfig.getInt("Character.stats.extraWisdom");
		int extraCharisma = fileconfig.getInt("Character.stats.extraCharisma");
		
		int maxhealth = fileconfig.getInt("Character.stats.maxhealth");
		int health = fileconfig.getInt("character.stats.health");
		int temporaryhealth = fileconfig.getInt("Character.stats.temporaryhealth");
		
		World world = Bukkit.getWorld(UUID.fromString(fileconfig.getString("Character.location.World")));
		double x = fileconfig.getDouble("Character.location.x");
		double y = fileconfig.getDouble("Character.location.y");
		double z = fileconfig.getDouble("Character.location.z");
		float yaw = (float) fileconfig.getDouble("Character.location.yaw");
		float pitch = (float) fileconfig.getDouble("Character.location.pitch");
		
		Location loc = new Location(world, x, y, z, yaw, pitch);
		
		int level = fileconfig.getInt("Character.stats.level");
		int experience = fileconfig.getInt("Character.stats.experience");
		
		String name = fileconfig.getString("Character.description.name");
		String title = fileconfig.getString("Character.description.title");
		String description = fileconfig.getString("Character.description.description");
		String race = fileconfig.getString("Character.description.race");
		
		String background = fileconfig.getString("Character.description.background");
		
		List<String> features = fileconfig.getStringList("Character.stats.features");
		
		boolean flag = fileconfig.getBoolean("Character.flag");
		
		List<String> buffs = fileconfig.getStringList("Character.stats.buffs");
		ArrayList<Buff> buff = new ArrayList<Buff>();
		for(String string : buffs) {
			buff.add(Buff.valueOf(string));
		}
		
		List<String> wearablesS = fileconfig.getStringList("Character.inventory.wearables");
		ItemStack[] wearables = new ItemStack[wearablesS.size()];
		for(int i = 0 ; i < wearablesS.size(); i++) {
			wearables[i] = InventoryConverter.getItem(wearablesS.get(i));
		}
		
		return new CharacterInfo(baseStrength, baseDexterity, baseConstitution, baseIntelligence, baseWisdom, baseCharisma,
				extraStrength, extraDexterity, extraConstitution, extraIntelligence, extraWisdom, extraCharisma,
				wearables, level, experience, name, title, description, race, background,
				maxhealth, health, temporaryhealth, features, buff, loc, flag);
	}
}
