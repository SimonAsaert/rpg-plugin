package tld.sima.rpgplugin.conversations;

import java.util.UUID;

import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;

import net.md_5.bungee.api.ChatColor;
import tld.sima.rpgplugin.Main;

public class RaceNameConversation extends StringPrompt {
	
	Main plugin = Main.getPlugin(Main.class);
	UUID uuid;
	
	public String getPromptText(ConversationContext context) {
		String message = ChatColor.GOLD + "Insert the name of the race you want to create. This can be changed. eg: " + ChatColor.WHITE + "human";
		return message;
	}
	
	public void setData(UUID uuid) {
		this.uuid = uuid;
	}

	public Prompt acceptInput(ConversationContext context, String input) {
		context.getForWhom().sendRawMessage(ChatColor.GOLD + "Changed your race name to: " + ChatColor.WHITE + input);
		plugin.getAdminMap().get(uuid).raceinfo.setName(input);
		
		return null;
	}

}
