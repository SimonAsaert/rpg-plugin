package tld.sima.rpgplugin.conversations;

import java.util.UUID;

import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;

import net.md_5.bungee.api.ChatColor;
import tld.sima.rpgplugin.Main;

public class CharacterNameConversation extends StringPrompt {
	
	Main plugin = Main.getPlugin(Main.class);
	
	UUID uuid;
	

	public Prompt acceptInput(ConversationContext context, String input) {
		
		plugin.getCharacterList().get(uuid).getList().get(plugin.getInventoryEventInfo().get(uuid).currentlylookedatcharacter).setName(ChatColor.translateAlternateColorCodes('&', input));
		
		context.getForWhom().sendRawMessage(ChatColor.GOLD + "Changed your character name to: " + ChatColor.WHITE + ChatColor.translateAlternateColorCodes('&', input));
		return null;
	}
	
	public void setData(UUID uuid) {
		this.uuid = uuid;
	}
	

	public String getPromptText(ConversationContext context) {
		String message = ChatColor.GOLD + "Insert the name you want your character to be. eg: " + ChatColor.WHITE + "Pelleas Thamior";
		return message;
	}
}
