package tld.sima.rpgplugin.conversations;

import java.util.UUID;

import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;

import net.md_5.bungee.api.ChatColor;
import tld.sima.rpgplugin.Main;
import tld.sima.rpgplugin.files.RaceSave;
import tld.sima.rpgplugin.inventories.RaceInformationMenu;

public class RaceStatChangeConversation extends StringPrompt{
	private enum statType {
		Str, Dex, Con,
		Int, Wis, Cha
	};
	private statType type;
	private UUID uuid;

	Main plugin = Main.getPlugin(Main.class);

	public Prompt acceptInput(ConversationContext con, String message) {
		try {
			int value = Integer.parseInt(message);
			switch(type) {
			case Str:
				plugin.getAdminMap().get(uuid).raceinfo.setStrength(value);
				break;
			case Dex:
				plugin.getAdminMap().get(uuid).raceinfo.setDexterity(value);
				break;
			case Con:
				plugin.getAdminMap().get(uuid).raceinfo.setConstitution(value);
				break;
			case Int:
				plugin.getAdminMap().get(uuid).raceinfo.setIntelligence(value);
				break;
			case Wis:
				plugin.getAdminMap().get(uuid).raceinfo.setWisdom(value);
				break;
			case Cha:
				plugin.getAdminMap().get(uuid).raceinfo.setCharisma(value);
				break;
			default:
				break;
			}
			RaceSave save = new RaceSave(plugin.getAdminMap().get(uuid).raceinfo.getFile());
			save.setupRace(plugin.getAdminMap().get(uuid).raceinfo);

			con.getForWhom().sendRawMessage(ChatColor.GOLD + "Changed stat to: " + ChatColor.WHITE + value);
				
		}catch (NumberFormatException e){
			con.getForWhom().sendRawMessage(ChatColor.RED + "You need to input a number value!");
		}
		RaceInformationMenu rim = new RaceInformationMenu();
		rim.newInventory(uuid, plugin.getAdminMap().get(uuid).raceinfo);
		return null;
	}

	public void setData(UUID uuid, String type) {
		this.type = statType.valueOf(type);
		this.uuid = uuid;
	}

	public String getPromptText(ConversationContext arg0) {
		String message = (ChatColor.GOLD + "Input value of the stat you wish to change. Eg: " + ChatColor.WHITE + "1");
		return message;
	}
}
