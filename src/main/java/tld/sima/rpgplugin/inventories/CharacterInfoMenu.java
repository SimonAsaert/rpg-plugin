package tld.sima.rpgplugin.inventories;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import tld.sima.rpgplugin.Main;
import tld.sima.rpgplugin.player.CharacterInfo;

public class CharacterInfoMenu {

	private Main plugin = Main.getPlugin(Main.class);
	
	private ItemStack createItem(ItemStack item, String disName, ArrayList<String> lore) {
		
		ItemMeta itemM = item.getItemMeta();
		itemM.setDisplayName(disName);
		itemM.setLore(lore);
		itemM.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		itemM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		itemM.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		item.setItemMeta(itemM);
		
		return item;
	}

	public void newInventory(UUID uuid, CharacterInfo info) {
		Inventory i = plugin.getServer().createInventory(null, 45, ChatColor.DARK_BLUE + "RPG Character Menu");
		ItemStack[] wearables = info.getWearables().clone();
		if (!wearables[0].getType().equals(Material.AIR)) {
			wearables[0] = createItem(wearables[0], ChatColor.WHITE + "Helmet", new ArrayList<String>());
		}else {
			ItemStack head = new ItemStack(Material.PLAYER_HEAD);
			SkullMeta meta = (SkullMeta) head.getItemMeta();
			meta.setOwningPlayer(Bukkit.getOfflinePlayer(uuid));
			head.setItemMeta(meta);
			wearables[0] = createItem(head, ChatColor.WHITE + "Helmet", new ArrayList<String>());
		}
		
		if (!wearables[1].getType().equals(Material.AIR)) {
			wearables[1] = createItem(wearables[1], ChatColor.WHITE + "Chestplate", new ArrayList<String>());
		}else {
			wearables[1] = createItem(new ItemStack(Material.LEATHER_CHESTPLATE), ChatColor.WHITE + "Chestplate", new ArrayList<String>());
		}
		
		if (!wearables[2].getType().equals(Material.AIR)) {
			wearables[2] = createItem(wearables[2], ChatColor.WHITE + "Leggings", new ArrayList<String>());
		}else {
			wearables[2] = createItem(new ItemStack(Material.LEATHER_LEGGINGS), ChatColor.WHITE + "Leggings", new ArrayList<String>());
		}
		
		if (!wearables[3].getType().equals(Material.AIR)) {
			wearables[3] = createItem(wearables[3], ChatColor.WHITE + "Boots", new ArrayList<String>());
		}else {
			wearables[3] = createItem(new ItemStack(Material.LEATHER_BOOTS), ChatColor.WHITE + "Boots", new ArrayList<String>());
		}
		
		if (!wearables[4].getType().equals(Material.AIR)) {
			wearables[4] = createItem(wearables[4], ChatColor.WHITE + "Offhand", new ArrayList<String>());
		}else {
			wearables[4] = createItem(new ItemStack(Material.STICK), ChatColor.WHITE + "Offhand", new ArrayList<String>());
		}
		ItemStack Strength;
		if(info.getBaseStrength() != -1) {
			int value = info.getBaseStrength() + info.getExtraStrength();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "" + value);
			Strength = createItem(new ItemStack(Material.SLIME_BALL), ChatColor.RED + "Strength: " + ChatColor.WHITE + " " + ChatColor.BOLD + ((value - 10)/2), lore);
		}else {
			Strength = createItem(new ItemStack(Material.SLIME_BALL), ChatColor.RED + "Roll Strength", new ArrayList<String>());
		}
		
		ItemStack Dexterity;
		if(info.getBaseDexterity() != -1) {
			int value = info.getBaseDexterity() + info.getExtraDexterity();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "" + value);
			Dexterity = createItem(new ItemStack(Material.SLIME_BALL), ChatColor.RED + "Dexterity: " + ChatColor.WHITE + " " + ChatColor.BOLD + ((value - 10)/2), lore);
		}else {
			Dexterity = createItem(new ItemStack(Material.SLIME_BALL), ChatColor.RED + "Roll Dexterity", new ArrayList<String>());
		}
		
		ItemStack Constitution;
		if(info.getBaseConstitution() != -1) {
			int value = info.getBaseConstitution() + info.getExtraConstitution();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "" + value);
			Constitution = createItem(new ItemStack(Material.SLIME_BALL), ChatColor.RED + "Constitution: " + ChatColor.WHITE + " " + ChatColor.BOLD + ((value - 10)/2), lore);
		}else {
			Constitution = createItem(new ItemStack(Material.SLIME_BALL), ChatColor.RED + "Roll Constitution", new ArrayList<String>());
		}
		
		ItemStack Intelligence;
		if(info.getBaseIntelligence() != -1) {
			int value = info.getBaseIntelligence() + info.getExtraIntelligence();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "" + value);
			Intelligence = createItem(new ItemStack(Material.SLIME_BALL), ChatColor.RED + "Intelligence: " + ChatColor.WHITE + " " + ChatColor.BOLD + ((value - 10)/2), lore);
		}else {
			Intelligence = createItem(new ItemStack(Material.SLIME_BALL), ChatColor.RED + "Roll Intelligence", new ArrayList<String>());
		}
		
		ItemStack Wisdom;
		if(info.getBaseWisdom() != -1) {
			int value = info.getBaseWisdom() + info.getExtraWisdom();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "" + value);
			Wisdom = createItem(new ItemStack(Material.SLIME_BALL), ChatColor.RED + "Wisdom: " + ChatColor.WHITE + " " + ChatColor.BOLD + ((value - 10)/2), lore);
		}else {
			Wisdom = createItem(new ItemStack(Material.SLIME_BALL), ChatColor.RED + "Roll Wisdom", new ArrayList<String>());
		}
		
		ItemStack Charisma;
		if(info.getBaseCharisma() != -1) {
			int value = info.getBaseCharisma() + info.getExtraCharisma();
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "" + value);
			Charisma = createItem(new ItemStack(Material.SLIME_BALL), ChatColor.RED + "Charisma: " + ChatColor.WHITE + " " + ChatColor.BOLD + ((value - 10)/2), lore);
		}else {
			Charisma = createItem(new ItemStack(Material.SLIME_BALL), ChatColor.RED + "Roll Charisma", new ArrayList<String>());
		}
		ItemStack name = createItem(new ItemStack(Material.NAME_TAG), ChatColor.GRAY + "Name: " + ChatColor.WHITE + info.getName(), new ArrayList<String>());
		ItemStack title = createItem(new ItemStack(Material.BOOK), ChatColor.GRAY + "Title: " + ChatColor.WHITE + info.getTitle(), new ArrayList<String>());
		ItemStack description = createItem(new ItemStack(Material.WRITTEN_BOOK), ChatColor.GRAY + "Description", new ArrayList<String>());
		StringBuilder builder = new StringBuilder();
		builder.append(ChatColor.WHITE).append("Health: ");
		if(info.getHealth() == info.getMaxHealth()) {
			builder.append(ChatColor.GREEN).append(info.getHealth()).append(ChatColor.WHITE).append("/").append(ChatColor.GREEN).append(info.getMaxHealth());
		}else if (info.getHealth() < (info.getMaxHealth()/4)) {
			builder.append(ChatColor.RED).append(info.getHealth()).append(ChatColor.WHITE).append("/").append(ChatColor.RED).append(info.getMaxHealth());
		}else {
			builder.append(ChatColor.WHITE).append(info.getHealth()).append("/").append(info.getMaxHealth());
		}
		ItemStack health = createItem(new ItemStack(Material.ROSE_RED), builder.toString(), new ArrayList<String>());
		ItemStack tmp = createItem(new ItemStack(Material.ORANGE_DYE), ChatColor.WHITE + "Temporary Health: " + info.getTemporaryHealth(), new ArrayList<String>());
		ItemStack level = createItem(new ItemStack(Material.EXPERIENCE_BOTTLE), ChatColor.WHITE + "Level: " + info.getLevel(), new ArrayList<String>());
		ItemStack experience = createItem(new ItemStack(Material.END_CRYSTAL), ChatColor.WHITE + "Experience: " + info.getExperience(), new ArrayList<String>());
		
		ItemStack back = createItem(new ItemStack(Material.BARRIER), ChatColor.RED + "Back", new ArrayList<String>());
		
		i.setItem(0, wearables[0]);
		i.setItem(1, name);
		
		i.setItem(3, Strength);
		i.setItem(4, Dexterity);
		i.setItem(5, Constitution);
		i.setItem(6, Intelligence);
		i.setItem(7, Wisdom);
		i.setItem(8, Charisma);
		
		i.setItem(9, wearables[1]);
		i.setItem(10, title);
		
		i.setItem(18, wearables[2]);
		i.setItem(19, description);
		
		i.setItem(21, health);
		i.setItem(22, tmp);
		
		i.setItem(24, level);
		i.setItem(25, experience);
		
		i.setItem(27, wearables[3]);
		
		i.setItem(36, wearables[4]);
		
		if(!info.getFlag()) {
			i.setItem(43, createItem(new ItemStack(Material.EMERALD_BLOCK), ChatColor.GREEN + "Select Character", new ArrayList<String>()));
		}
		
		i.setItem(44, back);
		Bukkit.getPlayer(uuid).openInventory(i);
	}
}
