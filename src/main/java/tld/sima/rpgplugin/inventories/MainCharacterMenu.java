package tld.sima.rpgplugin.inventories;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import tld.sima.rpgplugin.Main;
import tld.sima.rpgplugin.player.CharacterInfo;

public class MainCharacterMenu {
	
	private Main plugin = Main.getPlugin(Main.class);
	
	private ItemStack createItem(ItemStack item, String disName, ArrayList<String> lore) {
		
		ItemMeta itemM = item.getItemMeta();
		itemM.setDisplayName(disName);
		itemM.setLore(lore);
		itemM.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		itemM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		itemM.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		item.setItemMeta(itemM);
		
		return item;
	}

	public void newInventory(UUID uuid, UUID otherUUID, int insertionpoint) {
		Inventory i = plugin.getServer().createInventory(null, 27, ChatColor.DARK_BLUE + "RPG Main Character Menu");
		ArrayList<CharacterInfo> list = plugin.getCharacterList().get(otherUUID).getList();
		
		for(int j = 0 ; j < Math.min(list.size() - insertionpoint, 9); j++) {
			ItemStack[] wearables = list.get(j + insertionpoint).getWearables();
			ItemStack character;
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Name: " + ChatColor.WHITE + list.get(j+insertionpoint).getName());
			lore.add(ChatColor.GRAY + "Title: " + ChatColor.WHITE + list.get(j+insertionpoint).getTitle());
			if(wearables[0].getType().equals(Material.AIR)) {
				ItemStack head = new ItemStack(Material.PLAYER_HEAD);
				SkullMeta meta = (SkullMeta) head.getItemMeta();
				meta.setOwningPlayer(Bukkit.getOfflinePlayer(otherUUID));
				head.setItemMeta(meta);
				
				character = createItem(head, ChatColor.WHITE + "Character " + (j + insertionpoint), lore);
			}else {
				character = createItem(wearables[0], ChatColor.WHITE + "Character " + (j + insertionpoint), lore);
			}
			ItemStack delete = createItem(new ItemStack(Material.BARRIER), ChatColor.RED + "Delete " + (j + insertionpoint), lore);
			i.setItem(j, character);
			i.setItem(j+9, delete);
		}
		if((list.size() - insertionpoint) > 9) {
			ItemStack next = createItem(new ItemStack(Material.ARROW), ChatColor.GREEN + "Next page", new ArrayList<String>());
			i.setItem(18, next);
		}
		if(insertionpoint != 0) {
			ItemStack previous = createItem(new ItemStack(Material.ARROW), ChatColor.GREEN + "Previous page", new ArrayList<String>());
			i.setItem(18, previous);
		}
		if(uuid.equals(otherUUID)) {
			ItemStack createCharacter = createItem(new ItemStack(Material.EMERALD_BLOCK), ChatColor.GREEN + "Create New Character", new ArrayList<String>());
			i.setItem(26, createCharacter);
		}
		Bukkit.getPlayer(uuid).openInventory(i);
	}
}
