package tld.sima.rpgplugin.inventories;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import tld.sima.rpgplugin.Main;
import tld.sima.rpgplugin.server.RaceInformation;

public class RaceInformationMenu {

	private Main plugin = Main.getPlugin(Main.class);
	
	private ItemStack createItem(ItemStack item, String disName, ArrayList<String> lore) {
		
		ItemMeta itemM = item.getItemMeta();
		itemM.setDisplayName(disName);
		itemM.setLore(lore);
		itemM.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		itemM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		itemM.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		item.setItemMeta(itemM);
		
		return item;
	}
	

	public void newInventory(UUID uuid, RaceInformation info) {
		Inventory i = plugin.getServer().createInventory(null, 18, ChatColor.DARK_BLUE + "RPG Race Menu");
		
		ItemStack name = createItem(new ItemStack(Material.BOOK), ChatColor.GRAY + "Race: " + ChatColor.WHITE + info.getName(), new ArrayList<String>());
		
		ItemStack Strength = createItem(new ItemStack(Material.SLIME_BALL), ChatColor.GRAY + "Strength: " + ChatColor.WHITE + info.getStrength(), new ArrayList<String>());
		ItemStack Dexterity = createItem(new ItemStack(Material.SLIME_BALL), ChatColor.GRAY + "Dexterity: " + ChatColor.WHITE + info.getDexterity(), new ArrayList<String>());
		ItemStack Constitution = createItem(new ItemStack(Material.SLIME_BALL), ChatColor.GRAY + "Constitution: " + ChatColor.WHITE + info.getConstitution(), new ArrayList<String>());
		ItemStack Intelligence = createItem(new ItemStack(Material.SLIME_BALL), ChatColor.GRAY + "Intelligence: " + ChatColor.WHITE + info.getIntelligence(), new ArrayList<String>());
		ItemStack Wisdom = createItem(new ItemStack(Material.SLIME_BALL), ChatColor.GRAY + "Wisdom: " + ChatColor.WHITE + info.getWisdom(), new ArrayList<String>());
		ItemStack Charisma = createItem(new ItemStack(Material.SLIME_BALL), ChatColor.GRAY + "Charisma: " + ChatColor.WHITE + info.getCharisma(), new ArrayList<String>());
		
		ItemStack back = createItem(new ItemStack(Material.BARRIER), ChatColor.RED + "Back", new ArrayList<String>());
		
		ItemStack flag = createItem(new ItemStack(Material.EMERALD_BLOCK), ChatColor.WHITE + "Toggle flag: " + ChatColor.GRAY + info.getFlag(), new ArrayList<String>());
		
		i.setItem(0, name);
		
		i.setItem(2, Strength);
		i.setItem(3, Dexterity);
		i.setItem(4, Constitution);
		i.setItem(5, Intelligence);
		i.setItem(6, Wisdom);
		i.setItem(7, Charisma);
		
		i.setItem(9, flag);
		
		i.setItem(17, back);
		Bukkit.getPlayer(uuid).openInventory(i);
	}
}
