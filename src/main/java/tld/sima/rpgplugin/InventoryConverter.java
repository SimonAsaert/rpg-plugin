package tld.sima.rpgplugin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Builder;
import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.Repairable;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

public class InventoryConverter {
	
	static private String delim = "[|]";
	// Create my own private list of enchantment keys
	private static Map<String, Enchantment> enchantments;
	static {
		Map<String, Enchantment> map = new HashMap<String, Enchantment>();
		Enchantment[] enchs = Enchantment.values();
		for(Enchantment enchantment : enchs) {
			map.put(enchantment.getKey().getKey(), enchantment);
		}
		
		enchantments = Collections.unmodifiableMap(map);
	}
	
	// Convert Item meta to string
	private static StringBuilder metaToString(ItemStack item, StringBuilder builder) {
		ItemMeta meta = item.getItemMeta();
		builder.append("|");
		if (meta.hasDisplayName()) {
			builder.append(meta.getDisplayName());
		}else {
			builder.append("");
		}
		builder.append("|");
		if (meta.hasLore()) {
			Iterator<String> lore = meta.getLore().iterator();
			builder.append(lore.next());
			while (lore.hasNext()) {
				builder.append("`").append(lore.next());
			}
		}else {
			builder.append("");
		}
		
		return builder;
	}
	
	// Convert String to item Meta
	private static ItemStack stringToMeta(ItemStack item, String lore, String displayName) {
		ItemMeta meta = item.getItemMeta();
		if (!displayName.equals("")) {
			meta.setDisplayName(displayName);
		}
		if (!lore.equals("")) {
			ArrayList<String> lorearray = new ArrayList<String>();
			String[] lores = lore.split("[`]");
			for (String string : lores) {
				lorearray.add(string);
			}
			meta.setLore(lorearray);
		}
		item.setItemMeta(meta);
		return item;
	}
	
	// Convert Item to String
	public static String getString(ItemStack item) {
		StringBuilder builder = new StringBuilder();
		if (item == null) {
			builder.append(new ItemStack(Material.AIR).getType().name());
		}else {
			String type = item.getType().name();
			builder.append(type);
			
			if(item.getType().equals(Material.AIR)) {
				return builder.toString();
			}else if (item.getType().equals(Material.PLAYER_HEAD)) {
				SkullMeta meta = (SkullMeta) item.getItemMeta();
				builder.append("|").append(meta.getOwningPlayer().getUniqueId());
				builder = metaToString(item, builder);
			}else if (item.getType().equals(Material.WRITTEN_BOOK) || item.getType().equals(Material.WRITABLE_BOOK)) {
				BookMeta meta = (BookMeta) item.getItemMeta();
				builder.append("|");
				if (meta.hasAuthor()) {
					builder.append(meta.getAuthor());
				}else {
					builder.append("");
				}
				builder.append("|");
				if (meta.hasTitle()) {
					builder.append(meta.getTitle());
				}else {
					builder.append("");
				}
				builder = metaToString(item, builder);
				for (String page : meta.getPages()) {
					builder.append("|").append(page);
				}
			}else if (type.contains("BANNER")) {
				builder = metaToString(item, builder);
				BannerMeta meta = (BannerMeta) item.getItemMeta();
				List<Pattern> patterns = meta.getPatterns();
				for(Pattern pattern : patterns) {
					builder.append("|").append(pattern.getColor().name());
					builder.append("|").append(pattern.getPattern().name());
				}
			}else if (item.getType().equals(Material.FIREWORK_ROCKET)) {
				builder = metaToString(item, builder);
				FireworkMeta meta = (FireworkMeta) item.getItemMeta();
				builder.append("|").append(meta.getPower());
				List<FireworkEffect> effects = meta.getEffects();
				for(FireworkEffect effect : effects) {
					if(effect.hasFlicker()) {
						builder.append("|").append("FLICKER");
					}
					if(effect.hasTrail()) {
						builder.append("|").append("TRAIL");
					}
					
					if (!effect.getColors().isEmpty()) {
						List<Color> colours = effect.getColors();
						for (Color colour : colours) {
							builder.append("|").append("COLOUR").append(colour.asRGB());
						}
					}
					if(!effect.getFadeColors().isEmpty()) {
						List<Color> fades = effect.getFadeColors();
						for (Color colour : fades) {
							builder.append("|").append("FADE").append(colour.asRGB());
						}
					}
					builder.append("|").append("COMPLETE");
				}
			}else if (type.contains("POTION")) {
				builder = metaToString(item, builder);
				builder.append("|");
				PotionMeta meta = (PotionMeta) item.getItemMeta();
				if(meta.hasColor()) {
					builder.append(meta.getColor().asRGB());
				}else {
					builder.append("");
				}
				builder.append("|").append(meta.getBasePotionData().getType().name());
				builder.append("|").append(meta.getBasePotionData().isExtended());
				builder.append("|").append(meta.getBasePotionData().isUpgraded());
				if(meta.hasCustomEffects()) {
					List<PotionEffect> effects = meta.getCustomEffects();
					for (PotionEffect effect : effects) {
						builder.append("|").append(effect.getType().getName());
						builder.append("|").append(effect.getDuration());
						builder.append("|").append(effect.getAmplifier());
						builder.append("|").append(effect.isAmbient());
						builder.append("|").append(effect.hasParticles());
						builder.append("|").append(effect.hasIcon());
					}
				}
			}else if (type.contains("HELMET") || type.contains("CHESTPLATE") || type.contains("LEGGINGS") || type.contains("BOOTS") ||
					type.contains("PICKAXE") || type.contains("SHOVEL") || type.contains("SWORD") || type.contains("AXE") || type.contains("HOE")) {
				builder = metaToString(item, builder);
				builder.append("|").append(((Damageable) item.getItemMeta()).getDamage());
				builder.append("|").append(((Repairable) item.getItemMeta()).getRepairCost());
				
				builder.append("|");
				
				if(type.contains("LEATHER")) {
					LeatherArmorMeta colour = (LeatherArmorMeta) item.getItemMeta();
					builder.append(colour.getColor().asRGB());
				}else {
					builder.append("");
				}
				
				Map<Enchantment, Integer> ench = item.getItemMeta().getEnchants();
				for(Enchantment enchantment : ench.keySet()) {
					builder.append("|").append(enchantment.getKey().getKey()).append("|").append(ench.get(enchantment));
				}
			}else {
				builder = metaToString(item, builder);
			}
			builder.append("|").append(item.getAmount());
		}
		
		return builder.toString();
	}
	
	// Convert String to Item
	public static ItemStack getItem(String name) {
		String[] tokens = name.split(delim);
		ItemStack item = new ItemStack(Material.valueOf(tokens[0]));
		
		if (item.getType().equals(Material.AIR)){
			return item;
		}else if (item.getType().equals(Material.PLAYER_HEAD)) {
			SkullMeta meta = (SkullMeta) item.getItemMeta();
			meta.setOwningPlayer(Bukkit.getOfflinePlayer(UUID.fromString(tokens[1])));
			item = stringToMeta(item, tokens[2], tokens[3]);
		}else if (item.getType().equals(Material.WRITABLE_BOOK) || item.getType().equals(Material.WRITTEN_BOOK)) {
			BookMeta meta = (BookMeta) item.getItemMeta();
			if (!tokens[1].equals("")) {
				meta.setAuthor(tokens[1]);
			}
			if(!tokens[2].equals("")) {
				meta.setTitle(tokens[2]);
			}
			item.setItemMeta(meta);
			item = stringToMeta(item, tokens[3], tokens[4]);
			meta = (BookMeta) item.getItemMeta();
			for (int i = 5 ; i < tokens.length ; i++) {
				meta.addPage(tokens[i]);
			}
			item.setItemMeta(meta);
		}else if (tokens[0].contains("BANNER")) {
			item = stringToMeta(item, tokens[1], tokens[2]);
			BannerMeta meta = (BannerMeta) item.getItemMeta();
			for (int i = 3 ; i < (tokens.length - 2) ; i+=2) {
				meta.addPattern(new Pattern(DyeColor.valueOf(tokens[i]), PatternType.valueOf(tokens[i+1])));
			}
			item.setItemMeta(meta);
		}else if (item.getType().equals(Material.FIREWORK_ROCKET)) {
			item = stringToMeta(item, tokens[1], tokens[2]);
			FireworkMeta meta = (FireworkMeta) item.getItemMeta();
			meta.setPower(Integer.parseInt(tokens[3]));
			ArrayList<FireworkEffect> effects = new ArrayList<FireworkEffect>();
			Builder builder = FireworkEffect.builder();
			ArrayList<Color> colourlist = new ArrayList<Color>();
			ArrayList<Color> fadelist = new ArrayList<Color>();
			for (int i = 4 ; i < (tokens.length - 2) ; i++) {
				if (tokens[i].equals("TRAIL")){
					builder.trail(true);
				}else if (tokens[i].equals("FLICKER")) {
					builder.flicker(true);
				}else if (tokens[i].contains("FADE")) {
					fadelist.add(Color.fromRGB(Integer.parseInt(tokens[i].substring(4))));
				}else if (tokens[i].contains("COLOUR")) {
					colourlist.add(Color.fromRGB(Integer.parseInt(tokens[i].substring(6))));
				}else if (tokens[i].equals("COMPLETE")) {
					if(!colourlist.isEmpty()) {
						builder.withColor(colourlist);
						colourlist = new ArrayList<Color>();
					}
					if(!fadelist.isEmpty()) {
						builder.withFade(fadelist);
						fadelist = new ArrayList<Color>();
					}
					effects.add(builder.build());
					builder = FireworkEffect.builder();
				}
			}
		}else if (tokens[0].contains("POTION")) {
			item = stringToMeta(item, tokens[1], tokens[2]);
			PotionMeta meta = (PotionMeta) item.getItemMeta();
			if(!tokens[3].equals("")) {
				meta.setColor(Color.fromRGB(Integer.parseInt(tokens[3])));
			}
			PotionData data = new PotionData(PotionType.valueOf(tokens[4]), Boolean.parseBoolean(tokens[5]), Boolean.parseBoolean(tokens[6]));
			meta.setBasePotionData(data);
			for(int i = 7 ; i < (tokens.length - 2 ) ; i+= 6) {
				PotionEffect custom = new PotionEffect(PotionEffectType.getByName(tokens[i]), Integer.parseInt(tokens[i+1]), Integer.parseInt(tokens[i+2]), Boolean.parseBoolean(tokens[i+3]), Boolean.parseBoolean(tokens[i+4]), Boolean.parseBoolean(tokens[i+5]));
				meta.addCustomEffect(custom, true);
			}
			item.setItemMeta(meta);
		}else if (tokens[0].contains("HELMET") || tokens[0].contains("CHESTPLATE") || tokens[0].contains("LEGGINGS") || tokens[0].contains("BOOTS") ||
				tokens[0].contains("PICKAXE") || tokens[0].contains("SHOVEL") || tokens[0].contains("SWORD") || tokens[0].contains("AXE") || tokens[0].contains("HOE")) {
			item = stringToMeta(item, tokens[1], tokens[2]);
			((Damageable)item.getItemMeta()).setDamage(Integer.parseInt(tokens[3]));
			((Repairable)item.getItemMeta()).setRepairCost(Integer.parseInt(tokens[4]));
			
			if (tokens[0].contains("LEATHER")) {
				LeatherArmorMeta colour = (LeatherArmorMeta) item.getItemMeta();
				colour.setColor(Color.fromRGB(Integer.parseInt(tokens[5])));
				item.setItemMeta(colour);
			}
			
			for(int i = 6 ; i < (tokens.length - 2) ; i++) {
				Enchantment ench = enchantments.get(tokens[i]);
				item.addEnchantment(ench, Integer.parseInt(tokens[i+1]));
			}
		}else {
			item = stringToMeta(item, tokens[1], tokens[2]);
		}
		item.setAmount(Integer.parseInt(tokens[tokens.length - 1]));
		return item;
	}
}
