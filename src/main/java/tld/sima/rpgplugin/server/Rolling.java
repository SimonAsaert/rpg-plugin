package tld.sima.rpgplugin.server;

import java.util.ArrayList;

import tld.sima.rpgplugin.Main;

public class Rolling {
	
	static Main plugin = Main.getPlugin(Main.class);
	
	public static ArrayList<Integer> roll(int dicenumber, int sides){
		ArrayList<Integer> returnvalue = new ArrayList<Integer>();
		int finalValue = 0;
		int random;
		for(int i = 0 ; i < dicenumber ; i++) {
			random = (Math.abs(plugin.getRandom().nextInt())%sides) + 1;
			returnvalue.add(random);
			finalValue+= random;
		}
		returnvalue.add(finalValue);
		return returnvalue;
	}
}
