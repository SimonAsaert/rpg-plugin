package tld.sima.rpgplugin.server;

import java.io.File;
import java.util.ArrayList;

import tld.sima.rpgplugin.Main;
import tld.sima.rpgplugin.files.RaceSave;

public class RaceInformation implements Comparable<RaceInformation> {
	
	private Main plugin = Main.getPlugin(Main.class);
	// File location
	File file;
	// Name of race
	String name;
	// If this race is the default race
	boolean defaultFlag;
	// Stats
	int strength;
	int dexterity;
	int constitution;
	int intelligence;
	int wisdom;
	int charisma;
	// Current list of features
	ArrayList<String> features;
	
	public RaceInformation(String name, int strength, int dexterity, int constitution, int intelligence, int wisdom, int charisma, ArrayList<String> features, boolean flag) {
		this.name = name;
		this.strength = strength;
		this.dexterity = dexterity;
		this.constitution = constitution;
		this.intelligence = intelligence;
		this.wisdom = wisdom;
		this.charisma = charisma;
		this.features = features;
		this.defaultFlag = flag;
		this.file = new File(plugin.getDataFolder().toString() + File.separator + "Races" + File.separator + name + ".yml");

		if(!this.file.exists()) {
			RaceSave save = new RaceSave(name);
			save.setupRace(this);
		}
	}
	
	public RaceInformation() {
		this.name = "";
		this.defaultFlag = false;
		this.strength = 0;
		this.dexterity = 0;
		this.constitution = 0;
		this.intelligence = 0;
		this.wisdom = 0;
		this.charisma = 0;
		this.features = new ArrayList<String>();
		this.file = null;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!(obj instanceof RaceInformation)) return false;
		
		RaceInformation that = (RaceInformation) obj;
		return this.name.equals(that.name);
	}
	
	@Override
	public int hashCode() {
		return this.name.hashCode();
	}
	
	public int compareTo(RaceInformation that) {
		return this.name.compareTo(that.name);
	}
	
	public String getName() {				return name;}
	public int getStrength() {				return strength;}
	public int getDexterity() {				return dexterity;}
	public int getConstitution() {			return constitution;}
	public int getIntelligence() {			return intelligence;}
	public int getWisdom() {				return wisdom;}
	public int getCharisma() {				return charisma;}
	public boolean getFlag() {				return defaultFlag;}
	public ArrayList<String> getfeatures(){	return features;}
	public File getFile() {					return file;}

	public void setName(String value) {					
		this.name = value;
		if(this.file == null) {
			RaceSave save = new RaceSave(value);
			save.setupRace(this);
		}
		File newName = new File(plugin.getDataFolder().toString() + File.separator + "Races" + File.separator + value + ".yml");
		this.file.renameTo(newName);
	}
	public void setStrength(int value) {				this.strength = value;}
	public void setDexterity(int value) {				this.dexterity = value;}
	public void setConstitution(int value) {			this.constitution = value;}
	public void setIntelligence(int value) {			this.intelligence = value;}
	public void setWisdom(int value) {					this.wisdom = value;}
	public void setCharisma(int value) {				this.charisma = value;}
	public void setFeatures(ArrayList<String> value) {	this.features = value;}
	public void setFlag(Boolean value) {				this.defaultFlag = value;}
	public void setFile(File value) {					this.file = value;}
}
