package tld.sima.rpgplugin;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.conversations.Conversation;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;
import tld.sima.rpgplugin.commands.CharacterCommands;
import tld.sima.rpgplugin.commands.RaceCommands;
import tld.sima.rpgplugin.commands.RollingCommands;
import tld.sima.rpgplugin.events.GeneralEvents;
import tld.sima.rpgplugin.events.InventoryMenuInsertionEvents;
import tld.sima.rpgplugin.files.CharacterSave;
import tld.sima.rpgplugin.files.RaceSave;
import tld.sima.rpgplugin.player.AdminSetup;
import tld.sima.rpgplugin.player.CharacterInfo;
import tld.sima.rpgplugin.player.CharacterList;
import tld.sima.rpgplugin.player.InventoryEventInformation;
import tld.sima.rpgplugin.server.RaceInformation;

public class Main extends JavaPlugin {
	
	HashMap<UUID, CharacterList> playerCharacterList;
	HashMap<UUID, AdminSetup> adminList;
	HashMap<UUID, InventoryEventInformation> inventoryEventInfo;
	HashMap<String, RaceInformation> raceList;
	public HashMap<UUID, Conversation> conversations = new HashMap<UUID, Conversation>();
	private Random rm;
	
	@Override
	public void onEnable() {
		playerCharacterList = new HashMap<UUID, CharacterList>();
		adminList = new HashMap<UUID, AdminSetup>();
		raceList = new HashMap<String, RaceInformation>();
		File racedir = new File(this.getDataFolder().toString() + File.separator + "Storage" + File.separator + "Races");
		if(!racedir.exists()) {
			racedir.mkdirs();
		}
		File[] racefiles = racedir.listFiles();
		if(racefiles.length == 0) {
			this.getServer().getConsoleSender().sendMessage("I am creating a new default race");
			RaceInformation ri = new RaceInformation("Human", 1, 1, 1, 1, 1, 1, new ArrayList<String>(), true);
			RaceSave save = new RaceSave("human");
			save.setupRace(ri);
			raceList.put(ri.getName().toLowerCase(), ri);
		}else {
			for(File racefile : racefiles) {
				RaceSave racesave = new RaceSave(racefile);
				RaceInformation ri = racesave.getRace();
				raceList.put(ri.getName().toLowerCase(), ri);
			}
		}
		inventoryEventInfo = new HashMap<UUID, InventoryEventInformation>();
		this.rm = new Random();
		for(Player player : Bukkit.getOnlinePlayers()) {
			inventoryEventInfo.put(player.getUniqueId(), new InventoryEventInformation());
			String fileName = this.getDataFolder().toString() + File.separator + "Storage" + File.separator + "Characters" + File.separator + player.getUniqueId().toString();
			File check = new File(fileName);
			CharacterList list = new CharacterList(new ArrayList<CharacterInfo>());
			if(check.exists()) {
				File[] files = check.listFiles();
				for(File file : files) {
					CharacterSave save = new CharacterSave(file);
					CharacterInfo info = save.load();
					list.addToList(info);
				}
			}
			playerCharacterList.put(player.getUniqueId(), list);
		}
		CharacterCommands cc = new CharacterCommands();
		this.getCommand(cc.cmd1).setExecutor(cc);
		this.getCommand(cc.cmd2).setExecutor(cc);
		RollingCommands rc = new RollingCommands();
		this.getCommand(rc.cmd1).setExecutor(rc);
		RaceCommands rco = new RaceCommands();
		this.getCommand(rco.cmd1).setExecutor(rco);
		this.getCommand(rco.cmd2).setExecutor(rco);

		getServer().getPluginManager().registerEvents(new InventoryMenuInsertionEvents(), this);
		getServer().getPluginManager().registerEvents(new GeneralEvents(), this);
		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.AQUA + "RPG base Plugin enabled");
	}
	
	@Override
	public void onDisable() {
		for(Player player : Bukkit.getOnlinePlayers()) {
			UUID uuid = player.getUniqueId();
			CharacterList list = playerCharacterList.get(uuid);
			for(CharacterInfo info : list.getList()) {
				CharacterSave save = new CharacterSave(list.getList().indexOf(info) + "", uuid);
				save.save(info);
			}
		}
	}
	
	public HashMap<UUID, CharacterList> getCharacterList(){						return playerCharacterList;}
	
	public HashMap<String, RaceInformation> getRace(){							return raceList;}
	
	public Random getRandom() {													return rm;}
	
	public HashMap<UUID, InventoryEventInformation> getInventoryEventInfo() {	return inventoryEventInfo;}
	
	public HashMap<UUID, AdminSetup> getAdminMap(){								return adminList;}
	
}
